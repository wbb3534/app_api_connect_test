import 'package:app_api_connect_test/model/common_response.dart';
import 'package:app_api_connect_test/model/goods_create_request.dart';
import 'package:app_api_connect_test/model/goods_response.dart';
import 'package:app_api_connect_test/model/goods_update_request.dart';
import 'package:dio/dio.dart';

/// 레포지토리에서 api 연동을 한다...
class RepoGoods {
  // create Post api 구현
  Future<CommonResponse> setData(GoodsCreateRequest request) async {
    const String baseUrl = 'http://localhost:8080/v1/goods/new';

    Dio dio = Dio();

    final response = await dio.post(
        baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));

    return CommonResponse.fromJson(response.data);
  }
  // create Get api 구현
  Future<GoodsResponse> getList(String category) async {
    const String baseUrl = 'http://192.168.0.68:8080/v1/goods/list';

    Map<String, dynamic> params = {};
    params['category'] = category;

    Dio dio = Dio();

    final response = await dio.get(
        baseUrl,
        queryParameters: params,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));

    return GoodsResponse.fromJson(response.data);
  }
  // create Put api 구현
  Future<CommonResponse> putData(int id, GoodsUpdateRequest request) async {
    const String baseUrl = 'http://localhost:8080//v1/goods/update/{id}';

    Dio dio = Dio();

    final response = await dio.put(
        baseUrl.replaceAll('{id}', id.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));

    return CommonResponse.fromJson(response.data);
  }
  // create Del api 구현
  Future<CommonResponse> delData(int id) async {
    const String baseUrl = 'http://localhost:8080//v1/goods/del/{id}';

    Dio dio = Dio();

    final response = await dio.delete(
        baseUrl.replaceAll('{id}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));
    return CommonResponse.fromJson(response.data);
  }
}
