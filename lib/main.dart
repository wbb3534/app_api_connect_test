import 'package:app_api_connect_test/pages/page_list.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'pages/page_index.dart';

void main() {
  SystemChrome.setPreferredOrientations(
      [
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown
      ]
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'api 연동 예',
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
      builder: BotToastInit(),
      navigatorObservers: [
        BotToastNavigatorObserver()
      ],
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const PageList(),
    );
  }
}

