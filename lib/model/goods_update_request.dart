class GoodsUpdateRequest {
  String category;
  String goodsName;
  double goodsPrice;
  double salePrice;

  GoodsUpdateRequest(this.category, this.goodsName, this.goodsPrice, this.salePrice);

  Map<String, dynamic> toJson() {
    Map<String, dynamic> result = Map<String, dynamic>();

    result['category'] = this.category;
    result['goodsName'] = this.goodsName;
    result['goodsPrice'] = this.goodsPrice;
    result['salePrice'] = this.salePrice;

    return result;
  }
}