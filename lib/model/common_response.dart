class CommonResponse {
  bool isSuccess;
  int code;
  String msg;

  CommonResponse(this.isSuccess, this.code, this.msg);

  factory CommonResponse.fromJson(Map<String, dynamic> json) {
    return CommonResponse(
        json['isSuccess'] as bool,
        json['code'],
        json['code']
    );
  }
}