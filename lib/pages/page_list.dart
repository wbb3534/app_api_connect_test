import 'package:app_api_connect_test/components/common/component_custom_loading.dart';
import 'package:app_api_connect_test/components/common/component_no_contents.dart';
import 'package:app_api_connect_test/components/common/component_notification.dart';
import 'package:app_api_connect_test/model/goods_item.dart';
import 'package:app_api_connect_test/repository/repo_goods.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

class PageList extends StatefulWidget {
  const PageList({Key? key}) : super(key: key);

  @override
  State<PageList> createState() => _PageListState();
}

class _PageListState extends State<PageList> {
  List<GoodsItem> _list = [];
  int _totalItemCount = 0;

  @override
  void initState() {
    super.initState();
    _getList('FOOD');
  }

  Future<void> _getList(String category) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoGoods().getList(category).then((res) {
      setState(() {
        _list = res.list;
        _totalItemCount = res.totalItemCount;
      });

      BotToast.closeAllLoading();
    }).catchError((err) {
      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();

      BotToast.closeAllLoading();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    if (_totalItemCount <= 0) {
      return SizedBox(
        height: MediaQuery.of(context).size.height,
        child: const ComponentNoContents(icon: Icons.accessibility, msg: '상품이 없습니다'),
      );
    } else {
      return ListView.builder(
        itemCount: _list.length,
        itemBuilder: (_, index) => Container(
          child: Text('[ ${_list[index].category} ] ${_list[index].goodsName} ${_list[index].goodsPrice}'),
        ),
      );
    }
  }
}
