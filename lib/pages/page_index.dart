import 'package:app_api_connect_test/model/goods_response.dart';
import 'package:app_api_connect_test/repository/repo_goods.dart';
import 'package:flutter/material.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {

  GoodsResponse? _response;

  @override
  void initState() {
    super.initState();
    _getList('FOOD');
  }

  Future<void> _getList(String category) async {
    await RepoGoods().getList(category).then((res) {
      setState(() {
        _response = res;
      });
    }).catchError((err) {
      print(err);
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Text(_response == null ? '기다리세요' : _response.toString()),
      ),
    );
  }
}



